<?php
/**
 * @file
 * Fivestar Integer module.
 */

/**
 * Implements hook_field_formatter_info().
 */
function fivestar_integer_field_formatter_info() {
  return array(
    'fivestar_integer_formatter' => array(
      'label' => t('Static Stars'),
      'description' => t('Displays the value as a static Fivestar widget'),
      'field types' => array('number_integer', 'number_decimal', 'number_float', 'list_integer', 'list_float'),
      'settings' => array(
        'stars' => 5, // The total number of stars this rating is out of.
        'maximum_value' => 100, // The maximum value of the underlying numeric field.
        'widget' => array('fivestar_widget' => NULL),
        'force_whole' => NULL,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function fivestar_integer_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element['widget'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Star display options'),
    '#description' => t('Choose a style for your widget.'),
    '#weight' => -2,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $widgets = module_invoke_all('fivestar_widgets');

  $element['widget']['fivestar_widget'] = array(
    '#type' => 'radios',
    '#options' => array('default' => t('Default')) + $widgets,
    '#default_value' => isset($settings['widget']['fivestar_widget']) ? $settings['widget']['fivestar_widget'] : 'default',
    '#attributes' => array('class' => array('fivestar-widgets', 'clearfix')),
    '#pre_render' => array('fivestar_previews_expand'),
    '#attached' => array('css' => array(drupal_get_path('module', 'fivestar') . '/css/fivestar-admin.css')),
  );

  $element['stars'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of stars'),
    '#description' => t('Choose the number of stars to display the rating as a proportion of.'),
    '#default_value' => isset($settings['stars']) ? $settings['stars'] : 5,
    '#size' => 4,
  );

  $element['maximum_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum value'),
    '#description' => t('Choose the maximum value that will be entered in this field, i.e. the 100% value.'),
    '#default_value' => isset($settings['maximum_value']) ? $settings['maximum_value'] : 100,
    '#size' => 4,
  );

  $element['force_whole'] = array(
    '#type' => 'select',
    '#title' => t('Partial stars handling'),
    '#options' => array(
      NULL => t('Allow partial stars'),
      'floor' => t('Round down'),
      'ceil' => t('Round up'),
      'round' => t('Round toward the median'),
    ),
    '#description' => t('Only show whole stars, or allow \'partial\' (e.g. half) stars.'),
    '#default_value' => empty($settings['force_whole']) ? NULL : $settings['force_whole'],
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function fivestar_integer_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $widgets = module_invoke_all('fivestar_widgets');

  $summary = t("Style: @widget, Stars: @stars, Max value: @max_value, @partials.", array(
    '@widget' => isset($widgets[$settings['widget']['fivestar_widget']]) ? $widgets[$settings['widget']['fivestar_widget']] : t('default'),
    '@stars' => isset($settings['stars']) ? $settings['stars'] : 5,
    '@max_value' => isset($settings['maximum_value']) ? $settings['maximum_value'] : 100,
    '@partials' => empty($settings['force_whole']) ? t('Allow partial stars') : t('Force whole stars'),
  ));

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function fivestar_integer_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $widgets = &drupal_static(__FUNCTION__);
  if (!isset($widgets)) {
    $widgets = module_invoke_all('fivestar_widgets');
  }

  $elements = array();

  // Ensure some default values.
  $stars = isset($display['settings']['stars']) ? $display['settings']['stars'] : 5;
  $maximum_value = isset($display['settings']['maximum_value']) ? $display['settings']['maximum_value'] : 100;
  $divisions = empty($stars) ? 0 : (100 / $stars);

  // Find the desired widget.
  $widget_css = $display['settings']['widget']['fivestar_widget'];
  $widget_name = isset($widgets[$widget_css]) ? drupal_strtolower($widgets[$widget_css]) : 'default';
  $widget = array(
    'name' => $widget_name,
    'css' => $widget_css,
  );

  foreach ($items as $delta => $item) {
    $elements[$delta] = array(
      '#theme' => 'fivestar_static',
      '#rating' => ($item['value'] / $maximum_value) * 100, // Out of 100.
      '#stars' => $stars,
      '#widget' => $widget,
      // Manually include main Fivestar stylesheet, normally done by
      // theme_fivestar_formatter_default().
      '#attached' => array(
        'css' => array(
          drupal_get_path('module', 'fivestar'). '/css/fivestar.css'
        )
      )
    );

    if (!empty($display['settings']['force_whole']) && !empty($divisions)) {
      $function = function_exists($display['settings']['force_whole']) ? $display['settings']['force_whole'] : 'round';
      $elements[$delta]['#rating'] = $function($elements[$delta]['#rating'] / $divisions) * $divisions;
    }
  }
  return $elements;
}
